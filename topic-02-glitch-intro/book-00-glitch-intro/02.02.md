# Glitch Setup

If you do not have a github account, create one now:

- <a href ="https://github.com" target="_ blank">Github.com</a>

Now sign up for the Glitch service:

- <a href="https://Glitch.com" target="_ blank">Glitch.com</a>

This will require you to log in using your github account.

Once you are logged in:

- Choose `New Project` to start a new project, and ![](img/img1.PNG)


- From the dialog, choose the `hello-express` option to display a starter project: ![](img/img2.PNG)

A new project is opened with a random name (which we will change shortly).

![](img/img3.PNG)

Press the `Show` button and it will reveal a live version of of the app you have just created:

![](img/img4.PNG)

You can interact with this app by entering text in the Dreams box. 

