# Glitch Introduction

Upon completion of this lab you should be able to do all of the following:

## Anatomy of a Glitch project

- Set up and configure a Glitch account
- Understand the structure a Glitch project
- Share a link to a live project

## Editing & Downloading projects

- Modify the Javascript and html for a live project
- Be able to view and make some sense of the logs
- Download and run the project on your own workstation
- Export and view the project on github
