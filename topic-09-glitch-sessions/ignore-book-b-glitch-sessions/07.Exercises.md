# Exercise

This is the completed archive at this stage:

- <https://github.com/edeleastar/Glitch-playlist-4>

It can be imported into Glitch using `edeleastar/Glitch-playlist-4`

## Exercise 1

Test the application more comprehensively - signing up a range of users, and creating playlists. Make sure the users only see the playlists they have created.


## Exercise 2

Look at the `authenticate` method again:

~~~
  authenticate(request, response) {
    const user = userstore.getUserByEmail(request.body.email);
    if (user) {
      response.cookie('playlist', user.email);
      logger.info(`logging in ${user.email}`);
      response.redirect('/dashboard');
    } else {
      response.redirect('/login');
    }
  },
~~~

Can you see anything not quite right about it?

Hint: what happens if incorrect password entered? Try this now.

See if you can fix this problem - i.e. only allow user to log in if they provide correct password.

## Exercise 3

Version 2 of the Webmark assignment can be downloaded from here:

- <https://github.com/bmullally/webmark2>

In Glitch, create a new project, and import from github: `bmullally/webmark2`.

Using this lab as a guide - see if you can introduce sessions into webmark?

## Exercise 4

You should still have your own webmark assignment. Try the same now - bringing in the sessions feature into your solution.
