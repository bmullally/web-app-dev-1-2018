#Setup

The starting point for this lab is a website based on a computing programme:

- Download Archive: [Lab01a](archives/Lab01a.zip)

This is the project structure here:

~~~
└── public
    ├── assets
    │   └── images
    │       ├── automotive.png
    │       ├── banner.jpg
    │       ├── ctrg.png
    │       ├── iot
    │       │   ├── data
    │       │   │   ├── data-1.png
    │       │   │   ├── data-2.jpeg
    │       │   │   └── data-modules.png
    │       │   ├── devices
    │       │   │   ├── devices-1.png
    │       │   │   ├── devices-2.png
    │       │   │   └── devices-modules.png
    │       │   ├── maths
    │       │   │   ├── maths-1.png
    │       │   │   ├── maths-2.jpg
    │       │   │   └── maths-modules.png
    │       │   ├── networks
    │       │   │   ├── networks-1.jpeg
    │       │   │   ├── networks-1.png
    │       │   │   ├── networks-modules.png
    │       │   │   └── neworks-3.png
    │       │   ├── programming
    │       │   │   ├── programming-1.png
    │       │   │   ├── programming-2.jpeg
    │       │   │   └── programming-modules.png
    │       │   ├── project
    │       │   │   ├── project-1.png
    │       │   │   ├── project-2.jpeg
    │       │   │   └── project-modules.png
    │       │   └── timeline.png
    │       ├── tssg.png
    │       └── wit-crest.png
    ├── index.html
    ├── strands
    │   ├── data.html
    │   ├── devices.html
    │   ├── maths.html
    │   ├── networks.html
    │   ├── programming.html
    │   └── project.html
    └── style.css
~~~

Make sure you can see this structure in Windows Explorer / Mac Finder.