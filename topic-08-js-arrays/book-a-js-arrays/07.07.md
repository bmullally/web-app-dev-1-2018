#Update a Song

In our playlist app on Glitch, we can make changes to a song object by accessing the 'songs' array. First, we will make some changes to the listsongs.hbs partial, so that our list of songs now looks as follows:

![](img/edit.png)

Replace the code in listsongs.hbs with the following version:

##listsongs.hbs

```
<table class="ui fixed table">
  <thead>
    <tr>
      <th class="six wide">Song</th>
      <th class="six wide">Artist</th>
      <th class="two wide"> </th>
      <th class="two wide"> </th>
    </tr>
  </thead>
<tbody>
    {{#each playlist.songs}}
    <form class = "ui form" action="/playlist/{{../playlist.id}}/updatesong/{{id}}" method="POST">
    <tr class="fields">
      <td class="field">
        <span class="ui input">
         <input placeholder="Title" type="text" size = "30" name="title" autofocus required value = "{{title}}">
        </span>
      </td>
      <td class="field">
        <span class="ui input">
          <input placeholder="Artist" type="text" size = "30" name="artist" required value = "{{artist}}">
        </span>
      </td>
      <td>
        <button class="ui submit icon button"><i class="icon pencil alternate"></i></button>
      </td>
      <td>
        <a href="/playlist/{{../playlist.id}}/deletesong/{{id}}" class="ui icon button delsong">
          <i class="icon trash"></i>
        </a>
      </td>
    </tr>
    </form> 
    {{/each}}
  </tbody>
</table>
```

There have been a number of changes made, including:

- A `<form>` added in the body section of the table. The action of this form is the route to update a song
- The `<tr>` in the table body has the SemanticUI fields class applied, and each `<td>` cell has the field class applied
- The title and artist properties have been turned into form inputs so that they are now editable
- A submit button (pencil icon) has been added to each row; clicking this button saves the changes to that song
- Lastly, cell widths have been added in the `<th>` cells in the header section of the table

Examine these changes in the listsongs.hbs file. 


## routes.js

The following route should be added to the routes.js file:

```
router.post('/playlist/:id/updatesong/:songid', playlist.updateSong);
```


## playlist.js

Next, add the updateSong method in the playlist controller:

```
  updateSong(request, response) {
    const playlistId = request.params.id;
    const songId = request.params.songid;
    logger.debug("updating song " + songId);
    const alterSong = {
      title: request.body.title,
      artist: request.body.artist,
    };
    playlistStore.editSong(playlistId, songId, alterSong);
    response.redirect('/playlist/' + playlistId);
  },
```

## playlist-store.js

Finally, add the editSong method in the playlist-store.js file. 

```
  editSong(id, songId, songDetails) {
    const playlist = this.getPlaylist(id);
    const songs = playlist.songs;
    const thepos = songs.findIndex(field=> field.id === songId);
    songs[thepos].title=songDetails.title;
    songs[thepos].artist=songDetails.artist;
  },
```

Test the edit song function now to ensure that it works. You should see that any changes to title or artist are saved even after the page is refreshed. 