### Step 03 Solutions

**`myName`**

~~~
function myName(){
    var myName = 'Elie Schoppik';
    console.log(myName);
}
~~~

**`displayOddNumbers`**

~~~
var numbers = [1,2,3,4,5,6,7,8,9,10];
function displayOddNumbers(){
    for(var i = 0; i < numbers.length; i++){
        // if the value we are at in the array is not divisible by 2 (it's an odd number)
        if(numbers[i] % 2 !== 0){
            // print out that value!
            console.log(numbers[i]);
        }
    }
}
~~~

**`displayEvenNumbers`**

~~~
var numbers = [1,2,3,4,5,6,7,8,9,10];
function displayEvenNumbers(){
    for(var i = 0; i < numbers.length; i++){
        // if the value we are at in the array is divisible by 2 (it's an even number)
        if(numbers[i] % 2 === 0){
            // print out that value!
            console.log(numbers[i]);
        }
    }
}
~~~

**`returnFirstOddNumber`**

~~~
var numbers = [1,2,3,4,5,6,7,8,9,10];
function returnFirstOddNumber(){
    for(var i = 0; i < numbers.length; i++){
        if(numbers[i] % 2 !== 0){
            // print out that value, using return gets us out of the function!
            return numbers[i];
        }
    }
}
~~~

**`returnFirstEvenNumber`**

~~~
var numbers = [1,2,3,4,5,6,7,8,9,10];
function returnFirstEvenNumber(){
    for(var i = 0; i < numbers.length; i++){
        if(numbers[i] % 2 === 0){
            // print out that value!
            return numbers[i];
        }
    }
}
~~~

**`returnFirstHalf`**

~~~
var numbers = [1,2,3,4,5,6,7,8,9,10];
function returnFirstHalf(){
    return numbers.slice(0,numbers.length/2);
}
~~~

**`returnSecondHalf`**

~~~
var numbers = [1,2,3,4,5,6,7,8,9,10];
function returnSecondHalf(){
    return numbers.slice(numbers.length/2);
}
~~~

**` Write a function that uses a loop to iterate through the songs array and console.log all of the songs.`**

~~~
const playList = 
  {
    id: "01",
    title: "Beethoven Sonatas",
    artist: "Beethoven",
    songs: [ "Piano Sonata No. 3","Piano Sonata No. 7","Piano Sonata No. 10"],
    rating: 4
  };

function songOutput()
{
  for (let i=0;i<playList.songs.length; i++)
  {
    console.log(playList.songs[i]);
  }
}
~~~


**`Write another funciton that again uses a loop to iterate in the reverse direction, logging the people starting at 'James' and finishing with 'Greg'`**

~~~

function songOutputReverse()
{
  for (let i=playList.songs.length; i>-1; i--)
  {
    console.log(playList.songs[i]);
  }
}

~~~


**`write a function that uses the return keyword.`**
~~~

function songOutputReverseReturn(tempString)
{
   for (let i=playList.songs.length; i>0; i--)
  {
    tempString = tempString + " <br>" + playList.songs[i-1];
  }
  return tempString;
}
~~~

~~~
<html>
  <head>
    <title>JavaScript Test Site</title>
    <script src="functions-02.js"></script>
  </head>
  <body>
    <p>Nothing going on yet...</p>
    <a href="#" onClick="getPlaylists();">Display Playlists</a><hr>
    <a href="#" onClick="songOutput();">Output songs of Playlist 1</a><hr>
    <a href="#" onClick="songOutputReverse();">Output songs of Playlist 1 in reverse</a><hr>
    <div><script>document.write(songOutputReverseReturn(""))</script></div>
  </body>
</html>
~~~

