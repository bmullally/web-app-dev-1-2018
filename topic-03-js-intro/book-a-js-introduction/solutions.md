# Exercise 1:

~~~
const usersFirstName = prompt('Enter first name');
const usersLastName = prompt('Enter last name');
const usersAge = prompt('Enter age');

const user = {
  firstName: usersFirstName,
  lastName: usersLastName,
  age: usersAge,
};

console.log(user);
~~~

# Exercise 2:

~~~
const usersFirstName = prompt('Enter first name');
const usersLastName = prompt('Enter last name');
const usersAge = prompt('Enter age');
const userGreeting = prompt('Enter your greeting');

const user = {
  firstName: usersFirstName,
  lastName: usersLastName,
  age: usersAge,
  greeting: userGreeting,
  sayHello() {
    console.log(this.greeting + ' says ' + this.firstName);
  },
};

console.log(user);
user.sayHello();
~~~

# Exercise 3:

~~~
Have a go yourself!
~~~