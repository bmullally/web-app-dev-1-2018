# Template 1

Create a new project in Glitch; choose the hello-express option. Rename the project to your preference.

Turn on the logs viewer, and then import a project from github:

First, click 'tools' on the bottom-left of the window: 

![](img/01tools.png)

Select 'Git, Import and Export'. Click 'Grant Access' to give GitHub access to this project. Then, in the same menu, choose 'Import from Github'. 

![](img/02import.png)

Enter `rbirney/playlist1` as shown. This will replace your project completely:

![](img/02repo.png)

Your project should now look like this: 

![](img/02project.png)

Run the app and explore the (small) number of views:

![](img/03new.png)
![](img/04new.png)
![](img/05new.png)

